WITH merchant_areas AS (

SELECT
    merchantId,
    JSON_EXTRACT_ARRAY(areas) AS areas --extract array necessary to convert for exploding operation
FROM --TABLE HIDDEN FOR PRIVACY REASONS),

exploded_areas AS (
SELECT
    merchantId,
    r
FROM merchant_areas,
UNNEST(areas) r), --unnest necessary for further processing

area_properties AS (
SELECT
    merchantId,
    json_value(r, '$.name') AS area_name, --SELECTing specific values FROM JSON string
    json_value(r, '$.onlineBookable') AS onlineBookable, --SELECTing specific values FROM JSON string
FROM exploded_areas)

SELECT
    m.merchant_business_name,
    m.merchant_id,
    m.merchant_city_name,
    m.merchant_country_iso2,
    a.area_name
FROM --TABLE HIDDEN FOR PRIVACY REASONS, ALIAS: m, contains merchant data
JOIN area_properties a ON m.merchant_id = a.merchantId
WHERE onlineBookable = 'true'
ORDER BY merchant_business_name
