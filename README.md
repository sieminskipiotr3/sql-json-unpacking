## Application 

- This is a code snippet quickly showing on how to work with JSON string unpacking using SQL (BigQuery)

## Tech stack
- **Language**: SQL
- **Run Tool**: BigQuery

## Description

It shows three operations:
- Conversion to Array
- Exploding to have correct data in each row
- Extracting discretionary information based on JSON string log

